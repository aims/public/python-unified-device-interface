.. Python Boilerplate documentation master file, created by
   sphinx-quickstart on Fri Mar 19 11:48:18 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Python Unified Device Interface's documentation!
===========================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
