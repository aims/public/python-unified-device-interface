src.soil package
================

Submodules
----------

src.soil.component module
-------------------------

.. automodule:: src.soil.component
   :members:
   :undoc-members:
   :show-inheritance:

src.soil.docstring\_parser module
---------------------------------

.. automodule:: src.soil.docstring_parser
   :members:
   :undoc-members:
   :show-inheritance:

src.soil.element module
-----------------------

.. automodule:: src.soil.element
   :members:
   :undoc-members:
   :show-inheritance:

src.soil.error module
---------------------

.. automodule:: src.soil.error
   :members:
   :undoc-members:
   :show-inheritance:

src.soil.event module
---------------------

.. automodule:: src.soil.event
   :members:
   :undoc-members:
   :show-inheritance:

src.soil.figure module
----------------------

.. automodule:: src.soil.figure
   :members:
   :undoc-members:
   :show-inheritance:

src.soil.function module
------------------------

.. automodule:: src.soil.function
   :members:
   :undoc-members:
   :show-inheritance:

src.soil.measurement module
---------------------------

.. automodule:: src.soil.measurement
   :members:
   :undoc-members:
   :show-inheritance:

src.soil.object module
----------------------

.. automodule:: src.soil.object
   :members:
   :undoc-members:
   :show-inheritance:

src.soil.parameter module
-------------------------

.. automodule:: src.soil.parameter
   :members:
   :undoc-members:
   :show-inheritance:

src.soil.stream module
----------------------

.. automodule:: src.soil.stream
   :members:
   :undoc-members:
   :show-inheritance:

src.soil.variable module
------------------------

.. automodule:: src.soil.variable
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: src.soil
   :members:
   :undoc-members:
   :show-inheritance:
