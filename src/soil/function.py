import inspect
from typing import Any, Dict, List, Union, Callable

import rdflib

from .element import Element
from .error import InvokationException, NotImplementedException, ChildNotFoundException
from .figure import Figure
from .parameter import Parameter
from ..utils import root_logger
from ..utils.constants import HTTP_GET, HTTP_OPTIONS
from ..utils.error import SerialisationException, DeviceException

logger = root_logger.get(__name__)


class Function(Element):

    def __init__(self, uuid: str, name: str, description: str, arguments: List[Figure], returns: List[Figure],
                 implementation: Callable, ontology: str = None, profile: str = None):
        Element.__init__(self, uuid, name, description, ontology, profile)
        if uuid[:3] != 'FUN':
            raise Exception('{}: The UUID must start with FUN!'.format(uuid))
        if not isinstance(arguments, list):
            raise Exception('{}: Given arguments are not a list!'.format(uuid))
        for i in arguments:
            if not isinstance(i, Parameter):
                raise Exception('{}: Given argument is not of type Parameter!'.format(uuid))
        if not isinstance(returns, list):
            raise Exception('{}: Given returns are not a list!'.format(uuid))
        for o in returns:
            if not isinstance(o, Parameter):
                raise Exception('{}: Given return is not of type Parameter!'.format(uuid))

        self._arguments = arguments
        self._returns = returns
        self._implementation = implementation
        signature_arguments = {}
        for arg in self._arguments:
            signature_arguments[arg['uuid']] = f'arg_{arg["uuid"][4:].lower()}'
        self._signature = {'arguments': signature_arguments, 'returns': [ret['uuid'] for ret in self._returns]}
        # self._mqtt_callback = implementation['mqtt_callback'] if 'mqtt_callback' in implementation.keys() else None

    def __getitem__(self, item: Union[str, List[str]], method: int = HTTP_GET) -> Any:
        if item == "arguments":
            return self._arguments
        if item == "returns":
            return self._returns
        if isinstance(item, list):
            if len(item) == 0:
                return self
            everything = self._arguments + self._returns
            for o in everything:
                if o.uuid == item[0]:
                    return o[item[1:]]
            raise Exception(
                "{}: Given uuid {} is not the id of a child of the current component!".format(self.uuid, item))
        return super().__getitem__(item, method)

    def __setitem__(self, key: str, value: Any):
        if key == "arguments":
            if not isinstance(value, list):
                raise Exception('{}: Given arguments are not a list!'.format(self.uuid))
            if len(value) != len(self._arguments):
                raise Exception(
                    '{}: The number of given arguments does not match the number of required arguments!'.format(
                        self.uuid))
            for v in value:
                if not isinstance(v, Figure):
                    raise Exception('{}: Given argument is not of type Figure!'.format(self.uuid))
            self._arguments = value
        elif key == "returns":
            if not isinstance(value, list):
                raise Exception('{}: Given returns are not a list!'.format(self.uuid))
            if len(value) != len(self._returns):
                raise Exception(
                    '{}: The number of given returns does not match the number of required returns!'.format(self.uuid))
            for v in value:
                if not isinstance(v, Figure):
                    raise Exception('{}: Given return is not of type Figure!'.format(self.uuid))
            self._returns = value
        else:
            super().__setitem__(key, value)

    async def invoke(self, arguments: List[Figure], topic) -> Dict[str, List[Dict[str, Any]]]:
        returns = {"returns": []}
        args = {}
        if self._implementation is None:
            raise NotImplementedException(self._uuid, self._name)

        for a in arguments:
            var = self.__getitem__([a["uuid"]])
            Figure.check_all(var.datatype, var.dimension, var.range, a["value"])
            args[self._signature['arguments'][a["uuid"]]] = a["value"]

        # set up servers
        try:
            if inspect.iscoroutinefunction(self._implementation):
                # if self._mqtt_callback is not None:
                #     result = await self._implementation(functools.partial(self._mqtt_callback, topic), **args)
                # else:
                result = await self._implementation(**args)
            else:
                # if self._mqtt_callback is not None:
                #     result = self._implementation(functools.partial(self._mqtt_callback, topic), **args)
                # else:
                result = self._implementation(**args)
        except Exception as e:
            raise DeviceException(str(e), predecessor=e)

        if result is not None:
            # if only one element is returned encapsulate result with tuple to make for-loop working
            if len(self._signature['returns']) == 1:
                result = (result,)
            if len(result) != len(self._returns):
                raise InvokationException(self._uuid, self._name,
                                          "Internal Server Error. Function with UUID {} should return {} parameters, but invoked method returned {} values!".format(
                                              self.uuid, len(result), len(self._returns)))

            for value, uuid in zip(result, self._signature['returns']):
                var = [x for x in self._returns if x['uuid'] == uuid]
                if len(var) != 1:
                    raise InvokationException(self._uuid, self._name,
                                              "Internal Server Error. UUID {} of returned parameter does not match!".format(
                                                  uuid))
                else:
                    var = var[0]
                    Figure.check_all(var.datatype, var.dimension, var.range, value)
                    returns['returns'] += [{'uuid': uuid, 'value': value}]

        return returns

    def serialize(self, keys: List[str], legacy_mode: bool, method: int = HTTP_GET) -> Dict[str, Any]:
        if not keys or 'all' in keys:
            keys = ['uuid', 'name', 'description', 'arguments', 'returns', 'ontology']
        dictionary = super().serialize(keys, legacy_mode)
        if 'arguments' in keys:
            dictionary['arguments'] = list(
                map(lambda x: x.serialize(
                    ['name', 'uuid', 'description', 'datatype', 'value', 'dimension', 'range', 'ontology'], legacy_mode,
                    HTTP_OPTIONS),
                    self._arguments))
        if 'returns' in keys:
            dictionary['returns'] = list(
                map(lambda x: x.serialize(['name', 'uuid', 'description', 'datatype', 'dimension', 'ontology'],
                                          legacy_mode, HTTP_OPTIONS), self._returns))
        return dictionary

    @staticmethod
    def deserialize(dictionary: Dict[str, Any], implementation=None) -> 'Function':
        if 'uuid' not in dictionary:
            raise SerialisationException('The function can not be deserialized. UUID is missing!')
        uuid = dictionary['uuid']
        if uuid[:3] != 'FUN':
            raise SerialisationException(
                'The Function can not be deserialized. The UUID must start with FUN, but actually starts with {}!'.format(
                    uuid[:3]))
        if 'name' not in dictionary:
            raise SerialisationException('{}: The function can not be deserialized. Name is missing!'.format(uuid))
        if 'description' not in dictionary:
            raise SerialisationException(
                '{}: The function can not be deserialized. Description is missing!'.format(uuid))
        if 'arguments' not in dictionary:
            raise SerialisationException(
                '{}: The function can not be deserialized. List of arguments is missing!'.format(uuid))
        if 'returns' not in dictionary:
            raise SerialisationException(
                '{}: The function can not be deserialized. List of returns is missing!'.format(uuid))

        try:
            arguments = []
            for arg in dictionary['arguments']:
                arguments += [Parameter.deserialize(arg)]
        except Exception as e:
            raise SerialisationException('{}: An argument of the function can not be deserialized. {}'.format(uuid, e))
        try:
            returns = []
            for ret in dictionary['returns']:
                returns += [Parameter.deserialize(ret)]
        except Exception as e:
            raise SerialisationException('{}: A return of the function can not be deserialized. {}'.format(uuid, e))
        try:
            ontology = dictionary['ontology'] if 'ontology' in dictionary else None
            profile = dictionary['profile'] if 'profile' in dictionary else None
            return Function(dictionary['uuid'], dictionary['name'], dictionary['description'], arguments, returns,
                            implementation, ontology, profile)
        except Exception as e:
            raise SerialisationException('{}: The function can not be deserialized. {}'.format(uuid, e))

    def load_semantics(self, profiles_path: str, metadata_path: str, parent_name: str) -> None:
        # This method does nothing intentionally, as we do not have any semantic definition for function
        pass

    def serialize_semantics(self, kind: str, recursive=False) -> rdflib.Graph:
        # This method does nothing intentionally, as we do not have any semantic definition for function
        return None

    def resolve_semantic_path(self, suffix: str) -> (Element, str):
        # This method does nothing intentionally, as we do not have any semantic definition for function
        raise ChildNotFoundException('Could not resolve the semantic path.')

    @property
    def semantic_name(self) -> str:
        return ""
