import json
import pathlib
import sys
import pytest

repo_path = str(pathlib.Path(__file__).parent)
if repo_path not in sys.path:
    sys.path.insert(0, repo_path)

from src.soil.figure import Figure
from src.soil.error import DimensionException, TypeException


class TestFigure(object):

    @staticmethod
    @pytest.fixture
    def expected_result(request):
        number = request.node.funcargs['number']
        test = request.node.funcargs['test']
        file = pathlib.Path(request.config.rootdir, 'test', 'data', 'figure', f'{test}_results.json').with_suffix('.json')
        results = json.loads(file.read_text())
        return results[number]

    @staticmethod
    @pytest.fixture
    def parametrized_shape(request):
        number = request.node.funcargs['number']
        test = request.node.funcargs['test']
        file = pathlib.Path(request.config.rootdir, 'test', 'data', 'figure', f'{test}s.json').with_suffix('.json')
        shape = json.loads(file.read_text())
        return shape[number]

    @staticmethod
    @pytest.fixture
    def parametrized_value(request):
        number = request.node.funcargs['number']
        file = pathlib.Path(request.config.rootdir, 'test', 'data', 'figure', f'values.json').with_suffix('.json')
        data = json.loads(file.read_text())
        return data[number]

    @pytest.mark.parametrize('number', [i for i in range(15)])
    @pytest.mark.parametrize('test', ['scalar'])
    def test_is_scalar(self, number, test, parametrized_value, expected_result):
        result = Figure.is_scalar(parametrized_value)
        assert result == expected_result

    @pytest.mark.parametrize('number', [i for i in range(15)])
    @pytest.mark.parametrize('test', ['dimension'])
    def test_check_dimension(self, number, test, parametrized_value, parametrized_shape, expected_result):
        if expected_result:
            try:
                Figure.check_dimension(parametrized_shape, parametrized_value)
            except DimensionException as e:
                assert False, e
        else:
            with pytest.raises(DimensionException):
                Figure.check_dimension(parametrized_shape, parametrized_value)

    @pytest.mark.parametrize('number', [i for i in range(15)])
    @pytest.mark.parametrize('test', ['datatype'])
    def test_check_datatype(self, number, test, parametrized_value, parametrized_shape, expected_result):
        if expected_result:
            try:
                Figure.check_type(parametrized_shape, parametrized_value)
            except TypeException as e:
                assert False, e
        else:
            with pytest.raises(TypeException):
                Figure.check_type(parametrized_shape, parametrized_value)

